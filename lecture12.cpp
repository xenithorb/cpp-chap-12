#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>
using namespace std;

const char DATABASE[18] = "quarter_sales.txt";

struct DivisionVar
{
	char name[6];
	double quarter[4];
	double sales;
};

int main() 
{
	DivisionVar division[4];
	strcpy(division[1].name, "North");
	strcpy(division[2].name, "South");
	strcpy(division[3].name, "East");
	strcpy(division[4].name, "West");

	fstream database (DATABASE, ios::trunc);
 	database.write("",1);
	database<<"Division, Quarter1, Quarter2, Quarter3, Quarter4\n";
	database.close();
		
	for ( int x=1; x<5 ; ++x )
	{
		// acquire the data
		cout<<endl<<"Division: "<<division[x].name<<" Quarterly Sales Report\n\n"
			<<"Please enter quaterly sales figures for each quarter.\n\n";
		for ( int q=1; q<5 ; ++q )
		{
			cout<<"Quarter "<<q<<" Sales: ";
			cin>>division[x].quarter[q];
		}

		// write the acquired data
		fstream database (DATABASE, ios::app);
		database<<division[x].name<<",";
		for ( int q=1; q<5; ++q )
		{
			database<<division[x].quarter[x]<<",";
		}
		database<<"\n"; // register newline
		database.close();
	}

	system("pause");
	return 0;
}
